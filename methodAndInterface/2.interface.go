/**
	- Một kiểu interface là 1 tập các ký hiệu (thường là tên của method) của các method. Nó dùng để lưu trữ giá trị được implement bởi các method
		trên nhiều kiểu dữ liệu khác nhau (những kiểu dữ liệu có method dược liệt kê trong interface).
	- Features:
		+ interface value: Giá trị của interface có thể là bất kì giá trị nào có được bằng cách implement 1 trong số các method được định nghĩa trong
			interface. và nó là 1 tuple dạng (value, type) Xem ví dụ function describe ở dưới.
			Interface value chính là value của underlying concrete cùng với type của concrete value đó
		+ Interface định kiểu mạnh mẽ các method (tức là receiver của method được định nghĩa trong interface): Khi implement một interface
			phải implement trên đúng kiểu (pointer receiver hay value receiver)
		+ nil interface: là interface mà không có kiểu nào implement nó (nó có method mà không có kiểu dữ liệu nào implement nó)
		+ empty interface: Một empty interface có dạng {} tức là không có method nào, empty interface có thể chứa giá trị của bất kì kiểu nào.
		 	empty interface được sử dụng khá thường xuyên để xử lý những giá trị mà không xác định được kiểu cuả nó. ví dụ hàm fmt.Print nó nhận vào
		 	bất kì số lượng argument có kiểu là interface{}.
		 + type switch: coming soon, see below example
	- Operations:
		+ Type assertion: Cung cấp cách truy cập tới underlying value của interface value theo cú pháp: t := i.(T)
			Cú pháp trên có nghĩa là xác nhận (assert) rằng interface value i có hold concrete type T hay không và gán underlying T value cho biến t
			nếu i không hold T thì sẽ xảy ra panic error. để thuận tiện khi làm việc với interface, cụ thể là lấy giá trị của interface ta hay dùng
			cú pháp: t, ok := i.(T) cú pháp này sẽ trả về value của underlying concrete type của i và gán nó cho t đồng thời gán ok = true nếu
			i hold T ngược lại t = zero value của type T và ok = false

	- Notes:
		+ Khi dùng type assertion theo cú pháp: t, ok := i.(T) nếu i không hold T thì t = zero value của T, ok=false. Nếu không dùng ok
			tức là theo cú pháp: t := i.(t) thì sẽ xảy ra panic error


 */

package main

import (
	"fmt"
	"math"
)

// Define Abser interface
type Abser interface {
	Abs() float64
}

type NilInterface interface {
	M()
}

// Define struct
type MyFloat float64
type Vertex struct {
	X, Y float64
}

// Define Abs() method for MyFloat with value receiver
func (f MyFloat) Abs() float64 {
	if f < 0 {
		return float64(-f)
	}
	return float64(f)
}

// Define Abs() method on Vertex type with pointer receiver
func (v *Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func main() {
	// Define variables
	var a Abser
	f := MyFloat(-math.Sqrt2)
	v := Vertex{3, 4}

	// Implements Abser interface on f and v
	a = f
	fmt.Println("a.Abs() on f: ", a.Abs())
	a = &v
	fmt.Println("a.Abs() on &v: ", a.Abs())

	// Implement in golang is implicit: Ở đây kiểu Vertex sẽ implement interface Abser 1 cách ngầm định (implicit) vì
	// không có từ khóa implement xác định việc implement này như trong 1 vài ngôn ngữ khác.
	var ai Abser = &Vertex{2, 3}
	fmt.Println("ai.Abs() on Vertext: ", ai.Abs())
	describe(ai)

	// nil interface
	//var n NilInterface
	//n.M() // sẽ gây lỗi panic

	// empty interface
	var ei interface{}
	ei = 44
	fmt.Printf("Value: %v, Type: %T\n", ei, ei)
	ei = "Hello"
	fmt.Printf("Value: %v, Type: %T\n", ei, ei)

	// type assertion
	var ti interface{} = "Hello"
	ti_value, ok := ti.(string)
	ti_value_f, ok := ti.(float64)
	fmt.Println(ti_value, ok)
	fmt.Println(ti_value_f, ok)

	// type switches
	do(22)
	do("hello")
	do(float64(22))
	do(Vertex{3, 4})
}

// define a function print value and type of an interface
func describe(a Abser) {
	fmt.Printf("Value: %v, Type: %T\n", a, a)
}


// define function demonstrate  type switches
func do(i interface{}) {
	switch i.(type) {
	case int:
		fmt.Println("int")
	case string:
		fmt.Println("string")
	case bool:
		fmt.Println("bool")
	default:
		fmt.Println("unknow type")
	}
}