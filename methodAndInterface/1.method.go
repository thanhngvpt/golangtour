/**
	- Golang không có class tuy nhiên ta có thể định nghĩa các method cho các kiểu dữ liệu.
		Method là 1 function với 1 argument đặc biệt gọi là receiver.
	- Features:
		+ Về bản chất method là function nhân vào 1 argument đăc biệt gọi là receiver. và chính cái receiver này mới sinh ra nhiều vần đề.
		+ Receiver:
			- Pointer receiver: Method có thể chình sửa giá trị của biến có kiểu mà receiver point tới. Cách này được dùng thường xuyên hơn
				Value receiver.
			- Value receiver: Chỉ chỉnh sửa được bản sao của receiver
	- Note:
		+ Chỉ có thể định nghĩa các method cho các kiểu dữ liệu được định nghĩa trong package hiện tại, vì vậy nếu định nghĩa method cho
			các kiểu dựng sẵn sẽ không được vì chúng ở các package khác.
 */

package main

import (
	"fmt"
	"math"
)

type Vertex struct {
	X, Y float64
}

// Define Abs() method for Vertex
func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

// Định nghĩa 1 function có chức năng tương tự method Abs() trên
func Abs(v Vertex) float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func main() {
	v := Vertex{3, 4}
	fmt.Println("v.Abs(): ", v.Abs())
	fmt.Println("Abs(v): ", Abs(v))
}