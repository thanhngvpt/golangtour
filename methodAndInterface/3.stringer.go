package main

import (
	"fmt"
)

// define type name IPAddr has length is 4 and based byte type
type IPAddr [4]byte

// define String() method for interface stringer on IPAddr
//func (ip IPAddr) String() string {
//	ip_str := make([]byte, 4)
//	index := 0
//	for _, v := range ip {
//		index += copy(ip_str[index:], v)
//	}
//	return ip_str
//}

func (ip IPAddr) String() string {
	return fmt.Sprintf("%d.%d.%d.%d\n", ip[0], ip[1], ip[2], ip[3])
}

func main() {
	hosts := map[string]IPAddr{
		"loopback":  {127, 0, 0, 1},
		"googleDNS": {8, 8, 8, 8},
	}
	for name, ip := range hosts {
		fmt.Printf("%v: %v\n", name, ip)
	}
}