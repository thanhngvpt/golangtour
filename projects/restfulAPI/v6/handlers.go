package main

import (
	"fmt"
	"encoding/json"
	"net/http"
	"github.com/gorilla/mux"
)

// functions
func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Index page")
}

func TodoIndex(w http.ResponseWriter, r *http.Request) {
	todos := Todos{
		Todo{Name: "Learn basic"},
		Todo{Name: "Build a simple rest API"},
	}

	if err := json.NewEncoder(w).Encode(todos); err != nil {
		panic(err)
	}
}

func TodoShow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	todoId := vars["todoId"]

	fmt.Fprintf(w, "Todo show: ", todoId)
}
