package main

import (
	"fmt"
	"net/http"
)

// Functions
func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, %s, you are welcome", r.URL.Path[1:])
}

// main
func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8989", nil)
}
