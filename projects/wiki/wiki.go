package main

import (
	"html/template"
	"io/ioutil"
	"net/http"
	"regexp"
)

// data type
type Page struct {
	Title string
	Body []byte
}

// methods
func (p *Page) save() error {
	file_name := p.Title + ".txt"
	return ioutil.WriteFile(file_name, p.Body, 0600)
}

// functions
func load_page(title string) (*Page, error) {
	file_name := title + ".txt"
	body, err := ioutil.ReadFile(file_name)

	if err != nil {
		return nil, err
	}

	return &Page{Title: title, Body: body}, nil
}

func view_handler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := load_page(title)

	if err != nil {
		http.Redirect(w, r, "/edit/" + title, http.StatusFound)
	}

	render_template(w, "view", p)
}

func edit_handler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := load_page(title)

	if err != nil {
		p = &Page{Title: title}
	}

	render_template(w, "edit", p)
}

func save_handler(w http.ResponseWriter, r *http.Request, title string) {
	body := r.FormValue("body")
	p := &Page{Title: title, Body: []byte(body)}
	err := p.save()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/view/" + title, http.StatusFound)

}

var templates = template.Must(template.ParseFiles("edit.html", "view.html"))

func render_template(w http.ResponseWriter, tmpl string, p *Page) {
	err := templates.ExecuteTemplate(w, tmpl + ".html", p)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

var valid_path = regexp.MustCompile("^/(view|edit|save)/([a-zA-Z0-9])$")

func make_handler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := valid_path.FindStringSubmatch(r.URL.Path)

		if m == nil {
			http.NotFound(w, r)
			return
		}
		fn(w, r, m[2])
	}
}

// main
func main() {
	http.HandleFunc("/view/", make_handler(view_handler))
	http.HandleFunc("/edit/", make_handler(edit_handler))
	http.HandleFunc("/save/", make_handler(save_handler))

	http.ListenAndServe(":8989", nil)
}