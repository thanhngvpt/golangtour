/**
	- defer statement sẽ trì hoãn việc thực thi của 1 function cho tới khi surround function return (nếu không trong function nào thì chờ cho tới
	khi function main return)
	- Features:
		+ defer: là defer function call, vì vậy các argument được truyền vào function call này sẽ evaluated (được định gía) lúc defer function call,
					nhưng việc thực thi function call này sẽ được thực thi khi surround function return.
		+ Các function call được push vào 1 stack, tức là function call nào defer trước sẽ được thực thi sau cùng.

 */
package main

import "fmt"

func main() {
	fmt.Println("Hello")
	defer fmt.Println("World")
	fmt.Println("surround return")

	fmt.Println("Counting")
	for i := 0; i < 3; i++ {
		defer fmt.Println(i)
	}
	fmt.Println("Counting return")
}
