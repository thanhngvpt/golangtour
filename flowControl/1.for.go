/**
	- golang chỉ có 1 cấu trúc lặp duy nhất là for.
	- cú pháp: vòng lặp for gồm 3 thành phần phân cách nhau bởi dấu semicolon
		+ init statement: Thực thi trước lần lặp đầu tiên. Ta thường khai báo nhanh các biến cho for tại đây
		+ condition statement: Được đánh giá trước mỗi lần lặp
		+ the post statement: Thực thi ở cuối mỗi lần lặp
	- Features:
		+ range: kết hợp để loop trên slice, map
			- khi loop trên slice thì 1 lần loop(iteration) sẽ trả về 2 value 1 là index 2 là value tại index đó trong slice
	- Notes:
		+ Không có dấu ngoặc đơn parenthese bao quanh 3 thành phần của forloop, và dấu bracket luôn luôn phải có.
		+ Nếu condition không được thiết lập thì forloop sẽ trở loop forever
 */

package main

import "fmt"

func main() {

	// normal for
	for i := 0; i < 5; i++ {
		fmt.Println("Iterate number: ", i)
	}

	// forloop with init
	sum := 1
	for ; sum < 5; {
		sum += sum
	}
	fmt.Println("sum: ", sum)

	// forloop as while
	total := 3
	for total < 10 {
		total += total
	}
	fmt.Println("total: ", total)
}