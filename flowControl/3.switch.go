/**
	- Swith giống như if else nhiều lần với nhiều case.

 */
package main

import (
	"fmt"
	"runtime"
	"time"
)

func main() {
	fmt.Print("Your go is running on: ")
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Print("MacOS")
	case "linux":
		fmt.Print("GNU Linux")
	default:
		fmt.Printf("%s", os)
	}
	fmt.Println()

	fmt.Print("When is saturday? ")
	today := time.Now().Weekday()
	switch time.Saturday {
	case today + 0:
		fmt.Println("Today")
	case today + 1:
		fmt.Println("Tomorrow")
	default:
		fmt.Println("Too far away.")
	}

}