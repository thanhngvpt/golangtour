/**
	- Cấu trúc if trong golang cũng có phần giống với for.
	- Features:
		+ Có thể khởi tạo variables trước condition của if và các variables đó sẽ có scope trong if đó.
 */
package main

import (
	"fmt"
	"math"
)

func pow(x, n, lim float64) float64 {
	if v := math.Pow(x, n); v < lim {
		return v
	}

	return lim
}

func main() {
	fmt.Println(
		pow(3, 2, 10),
		pow(3, 2, 20),
	)
}