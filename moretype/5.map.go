/**
	- 1 map sẽ maps các key với các values tương ứng. map trong golang có thể coi như tương đồng với dict trong python, hash trong ruby ...
	- Syntax:
		+ var map_name map[type_of_key]type_of_value
		+ map_name := make(map[type_of_key]type_of_value): Trả về 1 map với kiểu được xác định trong hàm make, và initialized map đó.
		+ literal: giống struct literal
	- Features:
		+ zero value: là nil, 1 nil map không có key nào.

	- Operations: Ta có thể thao tác, insert, update, delete với map thông qua cú pháp map_name[key]
	- Note:
		+ Kiểm tra xem key có tồn tại trong map không: element, ok := map_name[key]

 */

package main

import (
	"fmt"
)

type Vertex struct {
	Lat, Long float64
}

// khai báo 1 biến map có tên mv với key kiểu string value kiểu Vertex
var mv map[string]Vertex

func main() {
	// Kiểu này có vẻ dài dòng quá
	mv = make(map[string]Vertex)
	mv["Bell lab"] = Vertex{
		40.68433, -74.39967,
	}
	fmt.Println(mv["Bell lab"])
	fmt.Println(mv)

	// khai báo literal map
	var m = map[string]Vertex{
		"Google Lab": Vertex{
			2435, -2346,
		},
		"Mircrosoft Lab": Vertex{
			3453, -6432,
		},
		"SpaceX Lab": Vertex{
			55555, 6666,
		},
	}

	m2 := map[string]Vertex {
		"IBM Lab": {
			2435, -2346,
		},
		"Audi Lab": {
			3453, -6432,
		},
		"BMW Lab": {
			55555, 6666,
		},
	}

	fmt.Println("m: ", m)
	fmt.Println("m2: ", m2)


}