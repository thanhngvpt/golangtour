/**
	- Ta đã biết array trong golang là fixed size làm cho array trong golang không được linh hoạt như array trong các
	 	ngôn ngữ lập trình khác. Tuy nhiên array trong golang lại chính là nền tảng cho 1 kiểu dữ liệu mà ta sẽ tìm hiểu sau đây gọi là slice.
	 	Thực tế thì slice trong golang tương tự như array trong các PL (programming language) khác.
	- Syntax:
		+ var <slice_name> []Type: Tạo slice theo cú pháp chuẩn và là cách duy nhất để tạo ra 1 nil slice.
		+ literal: <slice_name> := []{value_1, value_2, ...}: Tạo ra 1 slice theo cú pháp literal (tạo và gán giá trịs)
		+ make([]T, len, cap): Đây cũng là cách để tạo ra map và tạo ra dynamic-sized array (chả hiểu). Khi slice được tạo bằng cách này
			thì golang sẽ allocates a zeroed array và trả về 1 slice refers tới array này.
	- Features:
		+ slice trong golang không lưu trữ bất kì dữ liệu nào, nó chỉ mô tả những đoạn dữ liệu nằm ở underlying array.
		+ Thay đổi dữ liệu trong slice chính là thay đổi dữ liệu ở underlying array.
		+ Những slices cùng chia sẻ cùng 1 underlying array, khi thay đổi sẽ ảnh hưởng tới tất cả các slice khác.
		+ slice literal: Ta tạo 1 array literal theo cú pháp: <array_name> [size]Type, để tạo 1 slice literal ta cũng làm tương tự
		 	chỉ khác là không có size, về bản chất thì khi tạo slice literal thì golang sẽ tạo 1 array literal trước sau đó tạo ra 1 slice
		 	tham chiếu (reference) tới array đó.
		+ slice default: Khi ta slice 1 slice theo cú pháp slice[low:high] Ta có thể viết hoặc không viết chỉ số low và high, khi ta không
			xác định chỉ số này thì giá trị default sẽ được sử dụng như sau:
			- [:]: slice lấy tất cả các phần tử
			- [:n]: slice từ phần tử đầu tiên tới phần tử thứ n (exclude)
			- [n:]: slice lấy từ phần tử thứ n tới phần tử cuối cùng.
		+ 2 hàm quan trọng thường dùng với slice là:
			- len(slice_name): Trả về số phần tử thực có trong slice. Ta có thể thay đổi (extend) length của slice bằng cách reslice nó
			- cap(slice_name): Trả về số lượng phần tử tối đa mà underlying array có thể lưu trữ.
		+ reslice slice: sẽ tạo ra 1 slice mới cùng reference tới underlying array của slice ban đầu.
		+ nil slice: zero value của slice là nil, khi 1 slice là nil slice thì sẽ có len=0, cap=0 và không có underlying array
			làm thế nào để tạo ra nil slice? dùng var
		+ slice of slice: Cái này kiểu như mảng 2 chiều.
	- Opera1tion:
		+ append: đây là 1 thao tác khá phổ biến trong lập trình, để append thêm phần tử vào slice ta theo cú pháp sau:
			append(slice_name, value)

	- Note:
		+ Có một vấn đề rất quan ngại là khi slice 1 slice theo cú pháp [low:high] thì low sẽ tác động đến underlying array như sau:
			- Nếu low không được xác định (default là từ phần tử đầu tiên): thì vẫn giữ nguyên underlying array.
			- Nếu low được chỉ định và khác phần tử đầu tiên thì sẽ tạo ra 1 underlying array khác dẫn đến cap của slice sẽ khác.

 */
package main

import (
	"fmt"
	"reflect"
)

func main() {
	var array = [5]int {1, 2, 3, 4, 5}
	var int_slice []int

	fmt.Println(reflect.TypeOf(int_slice))
	fmt.Println(reflect.TypeOf(array))
	fmt.Println(int_slice)
	fmt.Println(array)

	fmt.Println("---------------slice demonstrate------------------")
	// slice demontrates
	names := [4]string{ // create array names
		"John",
		"Peter",
		"Alex",
		"George",
	}
	fmt.Println(names)

	a := names[1:3] 	// create slice a from element 1st (include) to 3rd (exclude)
	b := names[0:3]
	fmt.Println(a, b)

	b[1] = "David"
	fmt.Println(names)

	// slice literal
	int_slice_literal := []int{5, 6, 3, 4}
	fmt.Println(int_slice_literal)

	str_slice_literal := []string{"Hello", "World"}
	fmt.Println(str_slice_literal)

	struct_slice_literal := []struct {
		name string
		age int
		is_married bool
	}{
		{name: "John", age: 25, is_married: true},
		{name: "Peter", age: 28, is_married: false},
		{name: "Alex", age: 22, is_married: true},
		{name: "George", age: 33, is_married: true},
		{"Brown", 43, false},
		{name: "David", age: 24, is_married: false},
	}
	fmt.Println(struct_slice_literal)

	// change size of slice
	s := []int{1, 2, 3}
	fmt.Println(len(s), cap(s))

	s = s[1:2]
	fmt.Println(len(s), cap(s))

	// Tạo 1 underlying array có size là 5 và có kiểu int rồi tạo slice se tham chiếu đến underlying này
	se := []int{1, 2, 3, 4, 5}

	// in ra len và cap của slice se
	print_slice(se)

	// reslice chính se để thay đổi len của nó về 0
	se = se[:0]
	print_slice(se)

	// reslice se to Extend length của se: Đúng ra lúc này nó phải slice từ se có length=0 bên trên nhưng vì khi extend
	// slice length như này thì length mới này beyond qua cái length=0 cũ nên nó sẽ slice trực tiếp trên underlying array.
	se = se[:4]
	print_slice(se)

	// reslice lại se từ phần tử thứ 2 đến phần tử cuối cùng
	se = se[2:]
	print_slice(se)

	// nil slice
	var ns []int
	print_slice(ns)

	fmt.Println("---------- create slice use make----------")
	/// Create slice with built-in function make, chý ý là argument len là bắt buộc và cap tự động=len
	ms := make([]int, 5)
	print_slice(ms)

	fmt.Println("---------- create slice use make msc----------")
	// Chỉ định cap=5 len=0 cho hàm make
	msc := make([]int, 0, 5)
	print_slice(msc)

	// reslice msc từ phần tử đầu tiên tới phần cap của msc
	msc = msc[:cap(msc)]
	print_slice(msc)

	// reslice msc lấy từ phần tử thứ 1 tới cuối: Chưa hiểu tại sao tại bước reslice này lại tạo ra 1 underlying array khác?
	msc = msc[1:]
	msc2 := msc
	print_slice(msc)
	print_slice(msc2)

	fmt.Println("----------slice operations------------")
	var as []int
	print_slice(as)

	// append to nil slice
	as = append(as, 11)
	print_slice(as)
	as = append(as, 15)
	print_slice(as)
	as = append(as, 45, 32, 12, 55)
	print_slice(as)

	// sử dụng forloop trên slice bằng cấu trúc range sẽ trả về index và value tại index đó trong slice, để bỏ qua (không dùng) index hoặc value
	// trả về khi for range thì dùng dấu hyperphen (gạch dưới)
	var pow = []int {1, 2, 3, 5, 6}
	for i, v := range pow {
		fmt.Println("pow[", i, "]: ", v)
	}

	// skip value return from for range
	pow_pow := make([]int, 5)
	for i := range pow_pow {
		pow_pow[i] = 1 << uint(i) // 2**i (2^i)
	}
	for _, v := range pow_pow {
		fmt.Println("value: ", v)
	}
}

func print_slice(s []int) {
	if s == nil {
		fmt.Printf("slice: %v, address: %p, underlying array address: %p, len: %d, cap: %d, is nil slice\n", s, &s, *(&s), len(s), cap(s))
	} else {
		fmt.Printf("slice: %v, address: %p, underlying array address: %p, len: %d, cap: %d\n", s, &s, *(&s), len(s), cap(s))
	}
}