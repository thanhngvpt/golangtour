/**
	- struct là 1 kiểu dữ liệu do người dùng định nghĩa, chúng là 1 collection các field (nó có phần nào giống với class trong lập trình OO
		vì golang không hỗ trợ lập trình OO)
	- Cú pháp:
		type <struct_name> struct {
			<field_name_1> <Type>,
			<field_name_2> <Type>,
			...
			<field_name_n> <Type>
		}
	- Features:
		+ Truy cập các field của struct qua dấu chấm (dot)
		+ Ta có thể truy cập tới struct fields thông qua struct pointer như sau: p.field_name (đúng ra là ta phải viết
			là (*p.field_name) chú ý là (*p.field_name) không phải *p.field_name, nhưng golang hỗ trợ ta viết vậy để cho code đẹp và gọn hơn)
		+ struct literal: Khai báo các biến kiểu struct ngắn gọn, xem ví dụ dưới

 */
package main

import (
	"fmt"
)

// declare a struct
type Vertex struct {
	X, Y int
}

func main() {
	fmt.Println(Vertex{1, 2})

	v := Vertex{3, 4}
	fmt.Println("v.X: ", v.X)
	v.Y = 33
	fmt.Println("v: ", v)

	// struct pointer
	p2 := &Vertex{15, 25}
	fmt.Println(p2.X)
	fmt.Println((*p2).X)

	// struct literal
	var (
		v1 = Vertex{}
		v2 = Vertex{X: 0}
		v3 = Vertex{2, 3}
		pv = &Vertex{5, 6}
	)
	fmt.Println(v1, v2, v3, pv)
}