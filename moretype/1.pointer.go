/**
	- Golang cũng hỗ trợ kiểu pointer (con trỏ), pointer là 1 loại variable chỉ lưu trữ memory address của 1 variable khác.
	- Declare:
		+ var p *T: Khai báo pointer p trỏ tới kiểu T (và chỉ trỏ tới các biến kiểu T mà thôi)

	- Features:
		+ zero value (default value in some language programming): nil
		+ Có 2 toán tử khi dùng với biến:
			- &v: toán tử lấy địa chỉ của 1 biến, tạo ra 1 pointer trỏ tới biến v
			- *p: toán tử lấy giá trị lưu trong memory address mà p pointer trỏ tới.
 */
package main

import (
	"fmt"
	"reflect"
)

func main() {
	// Tạo biến i và gán giá trị 5
	i := 5

	// Tạo biến p trỏ tới địa chỉ của biến i
	p := &i

	// in ra địa chỉ của pointer p (không phải memory addres mà p trỏ tới)
	fmt.Println("memory address of pointer p(&p): ", &p)

	// in ra địa chỉ của biến i
	fmt.Println("memory address of i(&i): ", &i)

	// in ra kiểu của pointer p
	fmt.Println("type of pointer p: ", reflect.TypeOf(p))

	// in ra kiểu của pointer *int
	fmt.Println("type of *int(&i): ", reflect.TypeOf(&i))


	// in ra kiểu của giá trị được lưu trong memory addrest mà pointer *int trỏ tới
	fmt.Println("type of value stored in memory address &i (*int): ", reflect.TypeOf(*(&i)))

	// in ra giá trị được lưu trong  memory address mà pointer *int trỏ tới
	fmt.Println("Value stored in &i (*int pointed to): ", *(&i))


	// Toán tử & để lấy memory address của 1 biến chỉ áp dụng cho các biến thông thường còn với các biến pointer hoặc truyền vào địa chỉ của
	// 1 biến ta phải dùng
	// fmt.Printf("%p"): %p sẽ in ra address của pointer
	fmt.Printf(" address of &i: %p\n", &i)
	fmt.Printf("address of i %p\n", i)
	fmt.Printf("address of p: %p\n", p)
	fmt.Printf("address of &p: %p\n", &p)
}