/**
	- Ta đã tìm hiểu về function trước đây, tuy nhiên trong golang function là thứ khá mạnh và phần này ta sẽ tìm hiểu 2 vấn đề của function là:
		+ function value: cái này đại loại là truyền 1 function làm argument cho 1 function khác,
		+ function closure:
			- Closure là 1 function value có thể references các variables bên ngoạì thân của chính function đó, nó có thể access và assign
				tới referenced variables (các biến được nó(closure) tham chiếu). theo nghĩa này là function bound to variables

 */
package main

import "fmt"

// Định nghĩa function adder() sẽ trả về 1 closure
func adder() func(int) int {
	// hàm adder() này sẽ trả về 1 closure, closure chính là 1 môi trường bao gồm return function bên dưới và các biến trong scope add() mà
	// cái return function có thể tham chiếu đến
	sum := 0

	return func(x int) int {
		sum += x
		return sum
	}
}

func main() {
	pos, neg := adder(), adder()
	for i := 0; i < 5; i++ {
		// Cái này khá khó hiểu, closure nó sẽ nhớ được cái biến sum của lần lặp trước
		fmt.Println(
			pos(i),
			neg(-2*i),
		)
	}
}