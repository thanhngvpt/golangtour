/**
	- array là 1 danh sách các phần tử tuần tự nhau được lưu thông qua 1 biến.
	- Khai báo:
		+ var <array_name> [size]Type: Khai báo 1 array có tên array_name có kích thước size và có kiểu là Type và
			khởi tạo zero value cho các phần tử của mảng
		+ literal: <array_name> := [size]Type{value_1, value_2..., value_n}
	- Features:
		+ array trong golang không thể thay đổi được kích thước sau khi khai  báo.
 */
package main

import (
	"fmt"
)

func main() {
	var int_arr [3]int
	var str_arr [2]string
	var b_arr [5]bool

	fmt.Println(int_arr)
	fmt.Println(str_arr)
	fmt.Println(b_arr)

	str_arr[1] = "Hello"
	fmt.Println(str_arr)

	literal_array := [3]int {1, 2, 3}
	fmt.Println(literal_array)
	fmt.Println(&literal_array)
	fmt.Printf("%p\n", &literal_array)
}