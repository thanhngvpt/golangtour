/**
	1. Overview
		- Giữ golang code ở 1 workspace riêng
		- Một workspace gồm nhiều version control repositories
		- Mỗi repository chứa 1 hoặc nhiều package.
		- Mỗi package lại bao gồm 1 hoặc nhiều file mã nguồn go trong 1 thư mục.
		- Đường dẫn tới thư mục của package sẽ được sử dụng khi import.
		=> Điều này không giống các môi trường lập trình khác là mỗi project chứa trong 1 workspace riêng và các workspace này gắn chặt với
		version control repositories.

	2. Workspace:
		- Một workspace là 1 hệ thống thư mục phân cấp (directory hierarchy) với 3 thư mục gốc chính.
			+ src: chứa các file mã nguồn golang
			+ pkg: chứa các package objects
			+ bin: chứa các file thực thi.
			=> go tool sẽ build các source file và source package rồi cài đặt các file binaries (sau khi build) vào thư mục pkg và bin
		- src: thường chứa các VCS (Version Control repoSitories)
	3. GOPATH:
		- Biến môi trường GOPATH xác định vị trí workspace của bạn.
	4. import path:
		- Một import path là 1 unique string xác định 1 package. import path của 1 package chính là đường dẫn tới packpage đó bên trong workspace
		hoặc ở remote repository
		- Các package từ standard library có cú pháp import ngắn gọn ví dụ: "fmt", "net/http". Các package do người dùng tự viết nên được xác định
		1 base path riêng tránh xung đột với các package trong standard library hoặc package của người khác.
		- Nếu lưu trữ code ở 1 remote repository (hoặc có ý định) như github chẳng hạn thì nên chọn base path là github.com/user.
		- Ví dụ ta tạo 1 package trên github như sau (chú ý ta chỉ có ý định lưu code trên github.com tức là hiện tại code vẫn ở máy ta)
		ta tạo 1 thư mục $GOPATH/src/github.com/user/hello (thư mục hello chính là package hello) xong tạo file main.go trong package hello vừa tạo.
		Xon cài đặt package này bằng cách chạy lệnh go install github.com/user/hello. sẽ tạo ra 1 file thực thi trong bin
	5. Lib.
		- Tạo lib: mkdir $GOPATH/src/github.com/user/stringutil
		- Tạo file reverse.go trong thư mục stringutil xong chạy lệnh go install để build ra 1 file .a trong thư mục pk
	6. Remote path.
		- Khi ta muốn sử dụng một remote path ví dụ ở github.com ta phải sử dụng câu lệnh go get ví dụ go get github.com/golang/example/hello
			go get sẽ tự động fetch, build và install package đó

	Tổng kết:
		- Library:  là 1 file mà có dòng package <package_name> (không phải package main). và ta có thể import vào chương trình để sử dụng.
			Và nếu ta install bằng lệnh go install thì sẽ sinh ra 1 file .a trong thư mục $GOPATH/pkg/<base_path>/<lib_name>
		- Program: là chương trình (package main) và có thể import các package vào để sử dụng. nếu chạy lệnh go install sẽ tạo ra file thực thi trong
			thư mục $GOPATH/bin (hay còn gọi là execute command) và những file này bắt buộc phải có packge main
		- package: là một thư mục chứa các file mã nguồn golang thực thi những chức năng cùng lĩnh vực, các file trong cùng package
			bắt buộc phải có dòng khai báo tên packge giống nhau ở đầu file packge <packge_name>
 */