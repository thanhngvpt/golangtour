/**
	- Trong golang việc gán hay đổi kiểu cho các biến luôn yêu cầu
	làm tường minh theo cú pháp T(v), đổi kiểu của v sang kiểu T
 */
package main

import ("fmt"; "math")

func main() {
	var x, y int = 3, 4
	var f float64 = math.Sqrt(float64(x*x + y*y))
	var z uint = uint(f)
	fmt.Println(x, y, z)
}