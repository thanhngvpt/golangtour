/**
	- Các biến khi được khai báo và không được khởi tạo giá trị
	tường minh thì sẽ được gán zero value của kiểu của biến đó.
	Danh sách các zero value với các kiểu như sau:
		+ 0: number
		+ false: boolean
		+ "": string

 */
package main

import "fmt"

func main() {
	var i int
	var f float64
	var b bool
	var s string

	fmt.Printf("%v %v %v %q\n", i, f, b, s)
}