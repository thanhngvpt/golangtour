/**
 * - Mỗi chương trình golang được tạo bởi từ 1 hoặc nhiều packages.
 * - Chương trình sẽ bắt đầu chạy từ package main.
 * - Chương trình dưới đây sử dụng 2 package là fmt và rand
 * - Để cho thuận tiện tên của package là tên cuối cùng
 *		trong import path. ví dụ ở trên ta có package rand
 */

package main

import (
	"fmt"
	"math/rand"
)

func main() {
	fmt.Println("My favorite number is: ", rand.Intn(10))
}

