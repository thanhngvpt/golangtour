/**
	- Trong golang 1 name được exported nếu chữ cái
	đầu của nó được viết hoa. Ví dụ trong package math
	Ta có math.Pi thì Pi chính là 1 name được exported vì
	nó có chữ cái đầu là P được viết hoa.
	- Các tên trong 1 package được exported thì có thể
	truy cập được từ bên ngoài package đó, và ngươc lại
	các tên không được exported thì không thể truy cập
	ở bên ngoài package đó.
 */

package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println("pi = ", math.Pi)
}
