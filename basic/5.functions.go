/**
	- Function là 1 đoạn chương trình được tạo ra với
	mục đích chia để trị và tái sử dụng code. Một function
	có các vấn đề sau:
		+ function name: tên của function
		+ parameters: Các tham số hình thức (tham số lúc định nghĩa f)unction
		+ arguments: Các tham số thực được truyền vào function (lúc call hoặc invoke function).
	- Một function trong golang có thể nhận vào
	0 hoặc nhiều arguments.
	- Khi 2 hoặc nhiều parameters của function có cùng kiểu
	ta có thể bỏ qua kiểu của các tham số và chỉ cần
	xác định kiểu cho argument cuối cùng.
	- Multiple results: Function trong golang có thể trả về 1 hoặc nhiều
	kết quả. ví dụ function swap sau.
	- Named return values: Các giá trị trả về của hàm trong
	golang có thể được đặt tên, và chúng được coi như
	là các variables được defined ở top của function.
	Cú pháp return without arguments sẽ trả về các giá trị
	được đặt tên trong khi định nghĩa function, cú pháp  này
	gọi là naked và naked chỉ nên được dùng trong các short functions.

 */

package main

import "fmt"

// Functions defines
func add(x int, y int) int {
	return x + y
}

func swap(x, y string) (string, string) {
	return y, x
}

func split(sum int) (x, y int) {
	x = sum * 4 / 9
	y = sum - x
	return
}

func main() {
	fmt.Println("add(2, 3): ", add(2, 3))
	x := "Hello"
	y := "Word"
	a, b := swap(x, y)
	fmt.Println("swap 'Hello' and 'World', swap('Hello', 'World'): ", a, b)
	//fmt.Println("split(17): ", split(17))
	fmt.Println(split(17))
}