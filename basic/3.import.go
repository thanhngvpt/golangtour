/**
 * - Trong golang import là từ khóa dùng để import 1
 	package khác vào chương trình, ta có thể
 	import 1 hoặc nhiều package.
 	- Cú pháp:
 		+ import "package_name"
 		+ import (
 			"package_name_1"
 			"packge_name_2"
 		)
 */

package main

import "fmt"
import "math"

func main() {
	fmt.Printf("Now you have %g problems.\n", math.Sqrt(7))
}