/**
	- Trong golang ta khai báo variables bằng từ khóa var như sau:
	var <var_name_1>, <var_name_2> ... type
	- Từ khóa var có thể sử dụng trong package hoặc function level.
		khi ta khai báo theo cách này thì golang sẽ
		khai báo allocate memory và khởi tạo default
		value cho variables.
	- variables with initializers: Ta hoàn toàn có thể khai báo các biến
	và  khởi tạo giá trị cho các biến, khi ta khai báo biến và khởi tạo
	giá trị cho biến luôn ta có thể không cần xác định  type của
	variable, golang sẽ tự gán type cho biến theo kiểu của giá trị
	được khởi tạo.
	- Short variable declarations: Việc khai báo biến theo 2 cách
	trên có vẻ hơi dài dòng vì thế golang hỗ trợ thêm 1 kiểu khai báo variable
	ngắn ngọn như sau (cach này được sử dụng nhiều)
	- constants: Hằng trong golang được khai báo tương tự như variable
	nhưng thay vì từ khóa var ta dùng từ khóa const và 1 điều
	lưu ý là ta không thể dùng toán tử khai báo biến ngắn gọn :=
	cho khai báo hằng


 */
package main

import "fmt"

// declare variables and init its default value on package level
var c, python, java bool

// declare and initializer values
var ruby, perl = "Ruby", "Perl"

func main() {
	// declare variables and init its default value on function level
	var i int

	fmt.Println(i, c, python, java)
	fmt.Printf("%v\n", &i)
	fmt.Println(ruby, perl)

	// short variable declarations
	k := 3
	rust, haskell := "Rust", "Haskell"
	fmt.Println(k, rust, haskell)

}