package main

import (
	"github.com/garyburd/redigo/redis"
	"github.com/soveran/redisurl"
	"fmt"
	"golang.org/x/sys/unix"
)

func main() {
	conn, err := redisurl.Connect()
	if err != nil {
		fmt.Println("connect error")
	}
}