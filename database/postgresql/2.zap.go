package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"time"
)

const (
	DB_USER     = "dbadmin"
	DB_PASSWORD = "12345"
	DB_NAME     = "zap_dev"
)

type order struct {
	id 			int
	created 	time.Time
	state 		string
	price		float64
}

func main() {
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable",
		DB_USER, DB_PASSWORD, DB_NAME)
	db, err := sql.Open("postgres", dbinfo)
	checkErr(err)
	defer db.Close()

	rows, err := db.Query("SELECT id, created, state, price FROM  order_order")
	checkErr(err)
	rows2, err2 := db.Query("SELECT COUNT(*) FROM order_order")
	checkErr(err2)

	for rows2.Next() {
		var count int
		err := rows2.Scan(&count)
		checkErr(err)
		fmt.Println("Total: ", count)
	}

	var count int = 0
	for rows.Next() {
		count++
		var single_order order
		if err := rows.Scan(&single_order.id, &single_order.created, &single_order.state, &single_order.price); err != nil {
			log.Fatal(err)
		} else {
			fmt.Println("count:", count, "order: ", single_order)
		}
	}


}

//func checkCount(rows *sql.Rows, db *sql.DB) (count int) {
//
//	rows_query, err := db.Query("SELECT COUNT(*) FROM ? as rows_query", rows);
//	if err != nil {
//		log.Fatal(err)
//	}
//
//	for rows_query.Next() {
//		err:= rows.Scan(&count)
//		checkErr(err)
//	}
//	return count
//}

//func checkCount(rows *sql.Rows) (count int) {
//	for rows.Next() {
//		err:= rows.Scan(&count)
//		checkErr(err)
//	}
//	return count
//}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}