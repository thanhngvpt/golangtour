/**
	- Channel là 1 kiểu đường ống mà thông qua nó ta có thể gửi và nhận dữ liệu.
	- Syntax: ch := make(chan int)
	- Features:
		+ channel phải được khởi tạo trước khi sử dụng giống như map hoặc slice.
		+ theo mặc định việc gửi và nhận dữ liệu thông qua channel sẽ bị khóa cho tới khi
			dữ liệu trong channel đã được nhận thành công hoặc được gửi thành công. Điều này cho phép goroutine
			đồng bộ hóa mà không cần phải khóa tường minh hay dùng biến điều kiện.

	- Operations:
		+ buffered channels: tham số này được dùng khi khởi tạo channel bằng hàm make theo cú pháp: channel_name := make(chan type, buffer_size).
			- Chỉ gửi được vào buffered channel khi buffer vẫn chưa full. tức là nó sẽ block send vào khi buffer full
			- Chỉ nhận được từ buffered channel khi buffer có. Tức là nó block receive khi nó empty.
		+ range and close:
			- Một sender có thể close 1 channel để chỉ thị rằng không có giá trị nào được gửi ( và chỉ sender mới có thể close channel).
			các receivers có thể test khi nào channel bị close bằng cách thêm biến kiểm tra vào theo cú pháp: v, ok := <-ch.
			Khi send dữ liệu vào 1 close chanel sẽ gây panic error. Việc close channel bằng sender chỉ cần thiết khi muốn
			terminate for range trên channel.
			- range: sẽ đọc lần lượt value trong channel cho tới khi nó close (terminate by sender close channel)
		+ select: cú pháp select cho phép 1 goroutine đợi trên nhiều communication operations. select sẽ khóa cho tới khi 1 trong số những case
			có thể chạy và nó sẽ thực thi case đó, nó sẽ chọn random khi có nhiều cái ready.

	- Notes:
		+ Khi 2 goroutine chia sẻ dữ liệu với nhau qua cùng 1 chanel thì mặc định khi khởi tạo channel nó sẽ có buffer size = 1. Tức là chỉ
			có thể gửi hoặc nhận 1 luồng, vì thế nếu goroutine thứ nhất xong và đẩy dữ liệu vào channel, thì dữ liệu đó vẫn sẽ ở trong đó
			cho tới khi dữ liệu đó được lấy ra, và chỉ khi nó được lấy ra thì goroutine 2 mới có thể đẩy dữ liệu vào channel đó được

 */
package main

import (
	"fmt"

	"time"
)

// Hàm tính tổng của slice s rồi gửi vào channel c
func sum(s []int, c chan int) {
	sum := 0
	for _, v := range s {
		sum += v
	}
	c <- sum
}

func sum2(c chan int) {
	sum := 99
	time.Sleep(100 * time.Millisecond)

	c <- sum
}

func main() {
	s := []int{7, 2, 8, -9, 3, 0}
	c := make(chan int)

	go sum(s[:len(s)/2], c)
	//go sum(s[len(s)/2:], c)
	go sum2(c)

	// lệnh này chỉ chạy khi c có giá trị vì thế bắt buộc sẽ chạy sau khi 2 goroutine kết thúc
	x, y := <-c, <-c

	fmt.Println("x: ", x, " y: ", y)


	// buffered channel
	ch := make(chan int, 2)
	ch <- 1
	ch <- 2

	// Mỗi lần receive sẽ được 1 value
	fmt.Println(<-ch, <-ch)

	// range and close
	cf := make(chan int, 10)
	go fibonacci(cap(cf), cf)
	for i := range cf {
		fmt.Println(i)
	}

	// select
	cs := make(chan int)
	qs := make(chan int)
	go func() {
		for i:= 0; i < 10; i++ {
			fmt.Println(<-cs)
		}
		qs <-0
	}()

	fib_select(cs, qs)
}

// caculate fibonacci
func fibonacci(n int, c chan int) {
	x, y := 0, 1
	for i := 0; i < n; i++ {
		c<-x
		x, y = y, x + y
	}
	close(c)
}

// calc fibonacci with select
func fib_select(c, quit chan int) {
	x, y := 0, 1
	for {
		select {
		case c<-x:
			x, y = x, x+y
		case <-quit:
			fmt.Print("Quit")
			return
		}
	}
}