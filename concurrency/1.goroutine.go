/**
	- Một goroutine là 1 lightweight thread được quản lý bởi go runtime.
	- Syntax: go function_name(arguments)
	- Features:
		+ Mọi chương trình go đều có 1 goroutine chính, goroutine này bắt đầu khi hàm main chạy.
		+ Việc định giá (evaluate) của function_name và các argument xảy ra tại goroutine hiện tại, còn việc thực thi f
			xảy ra ở 1 goroutine mới.
		+ Các goroutines chạy trong cùng 1 address space, vì vậy để truy cập vào shared memory phải được đồng bộ.
	- Note:
		+ main goroutine co1 thể thoát trước khi các goroutine finish vì vậy cần phải xử lý.
 */
package main

import (
	"fmt"
	"time"
)

// In string s 5 lần mỗi lần cách nhau 10s
func say(s string) {
	for i:= 0; i < 5; i++ {
		time.Sleep(100 * time.Millisecond)
		fmt.Println(s)
	}
}

func main() {
	// Tạo ra 1 goroutine để chạy hàm say
	go say("World")

	// thực thi hàm say()
	say("Hello")
}